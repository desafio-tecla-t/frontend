import styled from 'styled-components';
import { lighten } from 'polished';

export const Container = styled.div`
  background: #0f024e;
  padding: 0 30px;
  box-shadow: 0 0 0 0.04em #000;
`;

export const Content = styled.div`
   height: 64px;
   max-width: 1080px;
   margin: 0 auto;
   display: flex;
   justify-content: space-between;  
   align-items: center;

   nav {
       display: flex;
       align-items: center;

       img {
           margin-right: 20px;
           padding-right: 20px;
           border-right: 1px solid #eee; 
       }

       a {
           font-weight: bold;
           color: #56dd85;
           transition: color 0.4s;

           &:hover {
                color: ${lighten(0.12, '#56dd85')}
           }
       }
   }

   aside {
       display: flex;
       align-items: center;
   }
`;

export const Profile = styled.div`
  display: flex;
  margin-left: 20px;
  padding-left: 20px;
  border-left: 1px solid #eee;

  div {
      text-align: right;
      margin-right: 10px;

      strong {
          display: block;
          color: #eee;
      }

      a {
          display: block;
          margin-top: 2px;
          font-size: 12px;
          color: #999;
      }

      
  }
  
  img {
          width: 32px;
          height: 32px;
          border-radius: 50%;
      }
`;