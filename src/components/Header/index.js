import React from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

import logo from '../../assets/logo.webp';

import { Container, Content, Profile } from './styles';

export default function Header() {
    const profile = useSelector(state => state.user.profile);

    const image = profile.avatar ? profile.avatar.url : "https://api.adorable.io/avatars/50/abott@adorable.png";

  return (
    <Container>
        <Content>
            <nav>
                <img src={logo} alt="TeclaT" />
                <Link to="/dashboard">DASHBOARD</Link>
            </nav>

            <aside>
                <Profile>
                    <div>
                        <strong>{profile.name}</strong>
                        <Link to="/profile">Meu perfil</Link>
                    </div>
                    <img src={image} alt="Vitor Silva" />
                </Profile>
            </aside>
        </Content>
    </Container>
  );
}
