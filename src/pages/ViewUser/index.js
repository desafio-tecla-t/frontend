import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FaSpinner } from 'react-icons/fa';
import { Link } from 'react-router-dom';

import api from '../../services/api';
import Container from '../../components/Container';
import { Loading, UserContainer } from './styles';

export default class ViewUser extends Component {
    static propTypes = {
        match: PropTypes.shape({
            params: PropTypes.shape({
                details: PropTypes.string,
            }),
        }).isRequired,
    };

    state = {
        user: {},
        loading: true,
    };

    async componentDidMount() {
        const { match } = this.props;

        const userId = decodeURIComponent(match.params.id);

        const user = await api.get(`users/${userId}`);

        this.setState({
            user: user.data,
            loading: false,
        })

    };


    render() {
        const { user, loading } = this.state;

        const image = user.avatar ? user.avatar.url :  "https://api.adorable.io/avatars/50/abott@adorable.png"

        if (loading) {
            return (
                <Loading> <FaSpinner color='#FFF' size={24} /> Carregando </Loading>)
        }

        return (
            <Container>
                <UserContainer>
                    <Link to='/dashboard'>Voltar aos usuários</Link>
                    <img src={image} alt={user.name} />
                    <h1>{user.name}</h1>
                    <p>{user.email}</p>
                </UserContainer>
            </Container>
        );
    };
};
