import styled, { keyframes, css} from 'styled-components';
import { darken } from 'polished';

const rotate = keyframes`
    from {
        transform: rotate(0deg);
    }

    to{
        transform: rotate(360deg);
    }
`;

export const Loading = styled.div`
    color: #fff;
    font-size: 30px;
    font-weight: bold;
    display: flex;
    justify-content: center;
    align-items: center;
    height: 100vh;

    svg {
            margin-right: 5px;
            animation: ${rotate} 2s linear infinite;
        }
`

export const UserContainer = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;

    a {
        color: #56dd85;
        font-size: 16px;
        text-decoration: none;
        transition: color 0.2s;

        &:hover {
            color: ${darken(0.09, '#56dd85')}
        }
    }

    img {
        width: 120px;
        border-radius: 50%;
        margin-top: 20px;
    }

    h1{
        font-size: 24px;
        margin-top: 10px;
    }

    p{
        margin-top: 5px;
        font-size: 14px;
        color: #666;
        line-height: 1.4;
        text-align: center;
        max-width: 400px;
    }
`;