import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { darken } from 'polished';

export const Container = styled.div`
  max-width: 600px;
  margin: 50px auto;

  display: flex;
  flex-direction: column;

  header {
      display: flex;
      align-self: center;
      align-items: center;

      button {
          border: 0;
          background: none;
      }

      strong {
          color: #fff;
          font-size: 24px;
          margin: 0 15px;
      }
  }

  ul {
      display: grid;
      grid-template-columns: repeat(2, 1fr);
      grid-gap: 15px;
      margin-top: 30px;
  }
`;

export const Time = styled.li`
    padding: 20px;
    border-radius: 4px;
    background: #fff;

    opacity: ${props => (props.past ? 0.6 : 1)};

    strong {
        display: block;
        color: ${props => (props.available ? '#999' : '#7159c1')};
        font-size: 20px;
        font-weight: normal;
    }

    span {
        display: block;
        margin-top: 3px;
        color: ${props => (props.available ? '#999' : '#666')};
    }
`;

export const List = styled.ul`
    list-style: none;
    margin-top: 30px;
    overflow-y: auto;
    max-height: 250px;

    ::-webkit-scrollbar {
        width: 0px;  
        background: transparent;
    }
    
    li {
        padding: 15px 0;
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        align-items: center;

        & + li {
            border-top: 1px solid #eee;
        }
    }
`;

export const ButtonsContainer = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    flex-direction: row;
    width: 25%;
`;

export const ButtonLink = styled(Link)`
            color:${props => props.color};
            text-decoration: none;

            &:hover {
                color: ${props => darken(0.2, props.color)}
            }
`;