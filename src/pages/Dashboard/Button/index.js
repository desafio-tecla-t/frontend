import styled from 'styled-components';
import { darken } from 'polished';

const Button = styled.button`
    color: ${props => props.color};
    text-decoration: none;
    background: none;
    border: 0;
    transition: color 0.2s;

    &:hover {
        color: ${props => darken(0.2, props.color)}
    }
`;

export default Button;