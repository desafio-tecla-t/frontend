import React, { useState, useEffect } from 'react';
import api from '../../services/api';
import { toast } from 'react-toastify';
import Container from '../../components/Container';
import Button from './Button';

import { List, ButtonsContainer, ButtonLink } from './styles';

export default function Dashboard() {

  const [userList, setUserList] = useState([]);
  useEffect(() => {
    async function loadSchedule() {
      const response = await api.get('users');

      setUserList(response.data.user);
    };
    loadSchedule();
  }, []);

  async function handleSubmit(id) {
    await api.delete(`/users/${id}`);

    const response = await api.get('/users');

    toast.success('Usuário excluído com sucesso.');

    setUserList(response.data.user);
  };

  return (
    <Container>
      <h1>Lista de usuários</h1>
      <List>
        {userList.map(user => (
          <li key={user.id}>
            <span>{user.name}</span>
            <ButtonsContainer>
              <ButtonLink color={"#56dd85"}  to={`/editUser/${encodeURIComponent(user.id)}`}>Editar</ButtonLink>
              <ButtonLink color={'#7159c1'} to={`/details/${encodeURIComponent(user.id)}`}>Ver</ButtonLink>
              <Button color={"#f64c75"} onClick={() => handleSubmit(user.id)} type="submit">Deletar</Button>
            </ButtonsContainer>
          </li>
        ))}
      </List>
    </Container>
  );
}
