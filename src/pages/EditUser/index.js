import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form, Input } from '@rocketseat/unform';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';

import api from '../../services/api';
import { Container, BackContainer, BackLink } from './styles';

export default class EditUser extends Component {
    static propTypes = {
        match: PropTypes.shape({
            params: PropTypes.shape({
                user: PropTypes.string,
            }),
        }).isRequired,
    };

    state = {
        user: {},
    };

    async componentDidMount() {
        const { match } = this.props;

        const userId = decodeURIComponent(match.params.id);

        const user = await api.get(`users/${userId}`);

        this.setState({
            user: user.data,
        });

    };

    async handleSubmit(data) {
        try {
            const { id, name, email, ...rest } = data;

            const profile = Object.assign(
                { id, name, email },
                rest.oldPassword ? rest : {}
            );

            console.log(profile)
            await api.put(`users/${profile.id}`, profile);

            toast.success('Usuário atualizado com sucesso!');
        } catch (err) {
            toast.error('Erro ao atualizar usuário, confira seus dados!');
        }
    }

    render() {
        const { user } = this.state;

        return (
            <Container>
                <h1>
                    Editar usuário
                </h1>
                <Form initialData={user} onSubmit={this.handleSubmit}>
                    <Input name="id" disabled />
                    <Input name="name" placeholder="Nome Completo" />
                    <Input name="email" type="email" placeholder="Seu endereço de email" />

                    <hr />

                    <Input type="password" name="oldPassword" placeholder="Senha atual" />
                    <Input type="password" name="password" placeholder="Nova senha" />
                    <Input type="password" name="confirmPassword" placeholder="Confirmação de senha" />

                    <button type="submit">Atualizar Perfil</button>
                </Form>
                <BackLink to='/dashboard'>
                    <BackContainer>
                        Cancelar
                    </BackContainer>
                </BackLink>

            </Container>
        );
    };
};
