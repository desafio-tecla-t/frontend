import styled from 'styled-components';
import { darken } from 'polished';

export const Container = styled.div`
    max-width: 600px;
    margin-left: 25%;
    margin-right: 25%;
    margin-top: 4%;
    height: 90vh;

    form {
        display: flex;
        flex-direction: column;

        input {
            background: rgba(255, 255, 255, 1);
            border: 0;
            border-radius: 4px;
            height: 44px;
            padding: 0 15px;
            color: #333;
            margin: 0 0 10px;

            &::placeholder {
                color: #333;
            }
        }

        a {
            color: #fff;
            margin-top: 15px;
            font-size: 16px;
            opacity: 0.8;

            &:hover {
                opacity: 1;
            }
        }

        span {
            color: #fb6f91;
            align-self: flex-start;
            margin: 0 0 10px;
            font-weight: bold;
        }

        hr {
            border: 0;
            height: 1px;
            background: rgba(255, 255, 255, 0.2);
            margin: 10px 0 20px;
        }

        button {
            margin: 5px 0 0;
            height: 44px;
            background: #64b078;
            font-weight: bold;
            color: #fff;
            border: 0;
            border-radius: 4px;
            font-size: 16px;
            transition: background 0.2s;

            &:hover {
                background: ${darken(0.03, '#64b078')}
            }
        }
    }

    > button {
            width: 100%;
            margin: 10px 0 0;
            height: 44px;
            background: #f64c75;
            font-weight: bold;
            color: #fff;
            border: 0;
            border-radius: 4px;
            font-size: 16px;
            transition: background 0.2s;

            &:hover {
                background: ${darken(0.08, '#f64c75')}
            }
        }
`;
